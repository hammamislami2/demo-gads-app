FROM golang:latest

# set environtment module
ENV GO111MODULE=on

# set worker for this app
WORKDIR /app

# copy file go.mod and go.sum to this workdir or /app
COPY go.mod .
COPY go.sum .

# download dependecy/library
RUN go mod download

# copy project file to workdir or /app
COPY . .

# build the app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build .

# set env for 
ENV HTTP_PORT=8080
EXPOSE 8080

ENTRYPOINT [ "/app/demo-gads-app" ]
